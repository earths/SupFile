from django.db import models
from django.contrib.auth.models import User as userModel


# Create your models here.
class File(models.Model):
    name = models.CharField(max_length=150)


class Directory(models.Model):
    name = models.CharField(max_length=150)


class User(models.Model):
    user = models.OneToOneField(userModel, on_delete=models.CASCADE)
    directory = models.OneToOneField(Directory, on_delete=models.CASCADE)
