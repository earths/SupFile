from django.conf.urls import url

from . import views  # import views so we can use them in urls.

urlpatterns = [  # "/store" will call the method "index" in "views.py"
    url(r'^$', views.index),
    url(r'^register', views.register),
    url(r'^login', views.login),
    url(r'^dashboard', views.dashboard),
    url(r'^logout', views.logout),
]
