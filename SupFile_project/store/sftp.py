import pysftp
import paramiko
import crypt

# you need to have install pysftp with pip
# adapt your ip - root username - root password

IP = '192.168.0.1'
USERNAME = 'root'
PASSWORD = 'supinfo'


# each file or folder declare in parameters have to be declare with them path
# exemple : "folder1/folder2/folder3/file1"

# initiate  sftp connection
# this method should not be use in another python file
def sftpBeginConnection(username, password):
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    sftp = pysftp.Connection(IP, username=username, password=password, cnopts=cnopts)
    print("Connected !")
    return sftp


# close sftp connection
# this method should not be use in another python file
def sftpEndConnection(sftp):
    print("Finish !")
    sftp.close()


# download a file from the remote server to the web server
def download(filename, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.get(filename)
    sftpEndConnection(sftp)


# upload a file from the web server to the remote server
def upload(filename, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.put(filename)
    sftpEndConnection(sftp)


# delete a file on the remote server
def deleteFile(filename, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.remove(filename)
    sftpEndConnection(sftp)


# create a directory on the remote server
def createFolder(foldername, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.mkdir(foldername)
    sftpEndConnection(sftp)


# return the content of a directory on the remote server
def getFolderContent(path, username, password):
    sftp = sftpBeginConnection(username, password)
    listdir = sftp.listdir(path)
    sftpEndConnection(sftp)
    return listdir


# rename a directory on the remote server
def renameDirectory(oldname, newname, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.rename(oldname, newname)
    sftpEndConnection(sftp)


# rename a file on the remote server
def renameFile(oldname, newname, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.rename(oldname, newname)
    sftpEndConnection(sftp)


# remove a file on the remote server
def removeFolder(foldername, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.rmdir(foldername)
    sftpEndConnection(sftp)


# add a user on the remote server
def addUser(username, password):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(IP, '22', USERNAME, PASSWORD)
    print("Connected")
    encPass = crypt.crypt(password, "22")
    ssh.exec_command('useradd -p ' + encPass + " -m -g san_users " + username)
    # ssh.exec_command('edquota -p PROTO ' + username)  # a enlever si pas sur la nouvelle infra de fred
    ssh.close()
    print("Finish")


# remove a file on the remote server
def removeFile(filename, username, password):
    sftp = sftpBeginConnection(username, password)
    sftp.remove(filename)
    sftpEndConnection(sftp)
