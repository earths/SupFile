from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from .models import User, Directory
from django.contrib.auth.models import User as userModel
from django.contrib.auth import authenticate, login as lg, logout as lgt
from .sftp import *


# Create your views here.

def index(request):
    template = loader.get_template('store/index.html')
    return HttpResponse(template.render(request=request))


def login(request):
    template = loader.get_template('store/login.html')

    if request.POST.get("send") is None:
        return HttpResponse(template.render(request=request))

    user = authenticate(
        username=request.POST.get("email"),
        password=request.POST.get("password")
    )

    if user is not None:
        lg(request, user)
        return redirect(dashboard)

    context = {
        'attempt': 'fail'
    }

    return HttpResponse(template.render(context, request=request))


def logout(request):
    lgt(request)
    return redirect(index)


def register(request):
    template = loader.get_template('store/register.html')
    if request.POST.get("send") is None:
        return HttpResponse(template.render(request=request))

    if userModel.objects.filter(username=request.POST.get("email")).exists():
        context = {
            'attempt': 'already_exist'
        }
        return HttpResponse(template.render(context, request=request))

    user = userModel(
        username=request.POST.get("email"),
        first_name=request.POST.get("firstName"),
        last_name=request.POST.get("lastName"),
        email=request.POST.get("email")
    )
    user.set_password(request.POST.get("password"))
    user.save()

    # todo = to remove , useless with the new architecture
    directory = Directory(
        name="root/" + request.POST.get("firstName") + "_" + request.POST.get("lastName")
    )
    directory.save()
    User(
        user=user,
        directory=directory
    ).save()

    lg(request, user)
    addUser(request.POST.get("email"), request.user.password)

    return redirect(dashboard)


@login_required
def dashboard(request):
    template = loader.get_template('store/dashboard.html')
    path = request.user.username
    if request.GET.get("path") is not None:
        path = request.GET.get("path")
    context = {
        'folders': ["File1","Dir1"]
    }
    return HttpResponse(template.render(context, request=request))
